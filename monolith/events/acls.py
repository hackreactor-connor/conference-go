import requests

import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo():  # this will only get the same image for now...
    url = "https://api.pexels.com/v1/photos/1563256"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)

    return {"picture_url": response.url}


def get_weather(state, city):
    print(city)
    url = "https://api.openweathermap.org/data/2.5/weather/"
    params = {
        "q": f"{city}, {state}, USA",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    temp = round(((temp - 273.15) * (9 / 5) + 32), 2)

    return {"description": description, "temp": temp}
